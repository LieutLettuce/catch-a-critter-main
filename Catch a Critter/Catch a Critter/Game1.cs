﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;

namespace Catch_a_Critter
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class Game1 : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        const int MAX_CRITTERS = 20; // all capitals mean its a constant
        Critter[] critterPool = new Critter[MAX_CRITTERS]; // an array of crtter generating 10 versions of it.
        Score ourScore; // a blank address for the player's score.
        Button ourButton = null; // A blank address for the player's Button.
        Timer gameTimer = null; //  A blank address for the player's Time.
        bool playing = false;

        const float SPAWN_DELAY = 3f; // time between critter spawns.
        const float GAME_LENGTH = 5f;
        float timeUntilNextSpawn = SPAWN_DELAY;
        int currentCritterIndex = 0;
        
        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

             // Call the score's function to load its content
             ourScore = new Score();
             ourScore.LoadContent(Content);

            // Create timer content and load it.
            gameTimer = new Timer();
            gameTimer.LoadContent(Content);
            gameTimer.SetTimer(GAME_LENGTH);
            

            gameTimer.ourTimerCallback += EndGame;

            // Call the button's function to load its content
            ourButton = new Button();
            ourButton.LoadContent(Content);


            // Set te function that will be called when the button is clicked
            // ( += means it adds to any existing functions set there)
            ourButton.ourButtonCallback += StartGame;

            // Initialise random number generator for critter spceies
            Random rand = new Random();

           // Create critter objects and load their content
           for (int i = 0; i < MAX_CRITTERS; ++ i)
            {
                Critter.Species newSpecies =(Critter.Species)rand.Next(0, (int)Critter.Species.NUM);
                
                //Creating a new critter and giving it a new score
                Critter newCritter = new Critter(ourScore, newSpecies);

                // Loading the content for the new critter (image/sfx)
                newCritter.LoadContent(Content);

                // Add the newly created critter to the pool
                critterPool[i] = newCritter;
            }



            IsMouseVisible = true;
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            // TODO: Add your update logic here

            if (playing ==true)
            {
                gameTimer.Update(gameTime);
                
                // Should new critters spawn?
                timeUntilNextSpawn -= (float)gameTime.ElapsedGameTime.TotalSeconds;
                if (timeUntilNextSpawn <= 0f)
                {
                    // Reset the spawn timer
                    timeUntilNextSpawn = SPAWN_DELAY;

                    //Spawn a new critter

                    //Spawn next critter in list
                    critterPool[currentCritterIndex].Spawn(Window);
                    ++currentCritterIndex;

                    // If we went past the end of the array, wrap back around to 0
                    if (currentCritterIndex >= critterPool.Length)
                    {
                        currentCritterIndex = 0;
                    }
                }

                //ourCritter.Input();
                foreach (Critter eachCritter in critterPool)
                {
                    // Loop Contents
                    eachCritter.Input();
                }
            }
            else
            {
                ourButton.Input();
            }

            

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.LightGoldenrodYellow);

            spriteBatch.Begin();

            foreach (Critter eachCritter in critterPool)
            {
                // Loop Contents
                eachCritter.Draw(spriteBatch);
            }

            ourButton.Draw(spriteBatch);
            ourScore.Draw(spriteBatch);
            gameTimer.Draw(spriteBatch);
           
            spriteBatch.End();

            base.Draw(gameTime);
        }
        void StartGame()
        {
            playing = true;
            gameTimer.StartTimer();
            ourScore.ResetScore();
        }

        void EndGame()
        {
            playing = false;

            // TODO: Show button
            ourButton.Show();

            // Despawn any critters
            foreach (Critter eachCritter in critterPool)
            {
                eachCritter.Despawn();
            }

            // Reset timer 
            gameTimer.SetTimer(GAME_LENGTH);
        }
    }
}
