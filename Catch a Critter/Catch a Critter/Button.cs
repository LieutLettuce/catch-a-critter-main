﻿using System;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework;


namespace Catch_a_Critter
{
    class Button
    {
        // ---------------------
        // Data
        // ---------------------
        Texture2D image = null;
        SoundEffect sound = null;
        Vector2 position = Vector2.Zero;
        bool visable = true;

        // Delegates
        // Delegate definition states what type of functions are allowed for this delegate.
        public delegate void OnClick();
        public OnClick ourButtonCallback;

        /*Vector2 screenCenter = new Vector2(
           Window.ClientBounds.Width / 2,
           Window.ClientBounds.Height / 2
           );
           */

        // ---------------------
        // Behaviour
        // ---------------------
        public void LoadContent(ContentManager content)
        {
            image = content.Load<Texture2D>("graphics/button");
            sound = content.Load<SoundEffect>("audio/wooV2");
        }
        // ---------------------
        public void Draw(SpriteBatch spriteBatch)
        {
            if (visable == true)
            {
                spriteBatch.Draw(image, position, Color.White);
            }
        }
        // ---------------------
        public void Input()
        {

            // Get the current status of the mouse
            MouseState currentstate = Mouse.GetState();

            // Get the bounding box of the critter
            Rectangle bounds = new Rectangle(
                (int)position.X,
                (int)position.Y,
                image.Width,
                image.Height);

            // Check if we have the left button held down over the button
            // if the button is visable
            if (currentstate.LeftButton == ButtonState.Pressed
                && bounds.Contains(currentstate.X, currentstate.Y)
                && visable == true)
            {
                // We Click the button
                sound.Play();

                //Hide the button
                visable = false;

                //START THE GAME
                if (ourButtonCallback != null)
                ourButtonCallback();
            }
        }
        // ---------------------
        public void Show()
        {
            visable = true;
        }
        // ---------------------

    }
}
