﻿using System;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework;

namespace Catch_a_Critter
{
    class Critter
    {
        // ---------------------
        // Type Definitions
        // ---------------------
        public enum Species
            // A special type of integer with only speicific values allowed, that have names
        {
            CROCODILE,  // = 0
            DUCK,       // = 1
            ELEPHANT,   // = 2

            // --
            
            NUM         // = 3
        }

        // ---------------------
        // Data
        // ---------------------

        Texture2D image = null;
        SoundEffect sound = null;
        Vector2 position = Vector2.Zero;
        bool alive = false;
        Score scoreObject = null;
        int critterValue = 10;
        Species ourSpecies = Species.DUCK;


        // ---------------------
        // Behaviour
        // ---------------------
        public Critter(Score newScore, Species newSpecies)
        {
            // Constructor called when the object is created
            // NO return type]
            // Helps decide how the object will be set up
            // acn have arguments (such as newScore)
            // this one lets us have access to the game's score. 
            scoreObject = newScore;
            ourSpecies = newSpecies;
        }
        // ---------------------
        public void LoadContent(ContentManager content)
        {
            switch(ourSpecies)
            {
                case Species.CROCODILE:
                    image = content.Load<Texture2D>("graphics/crocodile");
                    critterValue = 10;
                    break;
                case Species.DUCK:
                    image = content.Load<Texture2D>("graphics/duck");
                    critterValue = 100;
                    break;
                case Species.ELEPHANT:
                    image = content.Load<Texture2D>("graphics/elephant");
                    critterValue = 50;
                    break;
                default:
                    // This should never happen
                    break;
            }

            sound = content.Load<SoundEffect>("audio/wooV2");

        }
        // ---------------------
        public void Draw(SpriteBatch spriteBatch)
        {
            if (alive == true)
            {
                spriteBatch.Draw(image, position, Color.White);
            }
        }
        // ---------------------
        public void Spawn(GameWindow window)
        {

            // Set the critter to be alive | If it is displayed or not
            alive = true;

            // Determain the bounds for the random location of the critter.
            int positionYMin = 0;
            int positionXMin = 0;
            int positionYMax = window.ClientBounds.Height - image.Height;
            int positionXMax = window.ClientBounds.Width - image.Width;

            // Generate Random Location of the critter within the bounds.
            Random rand = new Random();
            position.X = rand.Next(positionXMin, positionXMax);
            position.Y = rand.Next(positionYMin, positionYMax);
        }
        // ---------------------
        public void Despawn()
        {
            // Set Critter to not alive in order to make it not draw and unclickable.
            alive = false;
        }
        // ---------------------
        public void Input()
        {

            // Get the current status of the mouse
            MouseState currentstate = Mouse.GetState();

            // Get the bounding box of the critter
            Rectangle critterBounds = new Rectangle(
                (int)position.X,
                (int)position.Y,
                image.Width,
                image.Height );

            // Check if we have the left button held down over the critter
            if (currentstate.LeftButton == ButtonState.Pressed 
                && critterBounds.Contains(currentstate.X, currentstate.Y)
                && alive == true)
            {
                // We Clicked the critter
                sound.Play();

                // Despawn the critter
                Despawn();

                // Add to the score(TODO)
                scoreObject.AddScore(critterValue);

            }
        }
    }
}
